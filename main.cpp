#include <iostream>
#include "stack.hpp"
#include "queue.hpp"

int main() {
    stack<int> s;
    queue<int> q;

    for (int i = 0; i < 15; i++) {
        s.push(i);
        q.push(i);
    }

    while (!s.empty()) {
        std::cout << s.pop() << '\n';
    };

    while(!q.empty()) {
        std::cout << q.pop() << '\n';
    }
}
