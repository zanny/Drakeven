#pragma once

#include <iostream>
#include <vector>

template <class T> class stack {
    std::vector<T> data;

public:
    void push(T t) {
        data.push_back(t);
    }

    T pop() {
        T val = top();
        if(!data.empty())
            data.pop_back();
        return val;
    }

    T top() {
        if(data.empty()) {
            std::cout << "Popping an empty stack!\n";
            return T {};
        }
        return data.back();
    }

    bool empty() {
        return data.empty();
    }
};
