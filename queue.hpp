#pragma once

#include <iostream>

template <class T> class queue {
    int size = 10;
    T *data = new T[size];
    int cursor = -1;
public:
    ~queue() {
        delete[] data;
    }

    void push(T t) {
        if (cursor >= size - 1) {
            T *expand = new T[size * 2];
            for(int i = 0; i < size; ++i)
                expand[i] = data[i];
            size *= 2;
            delete[] data;
            data = expand;
        }
        data[++cursor] = t;
    }

    T pop() {
        if(empty()) {
            std::cout << "Popping an empty queue!\n";
            return T {};
        }
        T val = data[0];
        for(int i = 0; i < cursor; ++i) {
            data[i] = data[i + 1];
        }
        cursor--;
        return val;
    }

    bool empty() {
        return cursor < 0 ? true : false;
    }
};
